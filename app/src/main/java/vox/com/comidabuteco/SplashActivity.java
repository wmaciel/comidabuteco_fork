package vox.com.comidabuteco;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.parse.Parse;


public class SplashActivity extends Activity {


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		setupHandlerCloseSplash();

        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);

        Parse.initialize(this,  getString(R.string.parse_app_id),  getString(R.string.parse_client_key));

    }


	private void setupHandlerCloseSplash() {
		Runnable runnable = new Runnable() {

			@Override
			public void run() {
			
				 if(validacaoInicio()){
                     acessarLogin();
                 }
			}
		};

		new Handler().postDelayed(runnable, 2000);
	}


	private boolean validacaoInicio() {
		//TODO - validacoes necessárias
			return true;

	}

    private void acessarLogin(){
        startActivity(new Intent(getApplication(), DispatchActivity.class));
        finish();
    }



}